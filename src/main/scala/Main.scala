import java.security._
import java.util.Random

import Node.{CreateGenesisBlock, MakeTransaction}
import akka.actor.{ActorRef, ActorSystem}
import akka.routing.BroadcastGroup

import scala.collection.mutable.ListBuffer
import scala.io.StdIn

object Main  {
  def main(args: Array[String]): Unit = {
    val blockchainSystem = ActorSystem("blockchainSystem")

    //allows generating random (unique) key pairs each time
    val random = SecureRandom.getInstance("NativePRNG", "SUN")
    val generator = KeyPairGenerator.getInstance("RSA")
    generator.initialize(512, random)
    var keyPairs = ListBuffer.empty[KeyPair]
    var paths = ListBuffer.empty[String]

    for (i <- 1 to 4) {
      val keyPair = generator.genKeyPair()
      keyPairs += keyPair
      paths += ("/user/" + keyPair.getPublic.toString
        .replace("Sun RSA public key, 512 bits\n  modulus: ", "")
        .replace("public exponent: 65537", "").trim)
    }
    var counter = 0
    val pathsImmutable = paths.toList
    lazy val router = blockchainSystem.actorOf(BroadcastGroup(pathsImmutable).props(), "router")
    println(router.path) //DO NOT TOUCH! (somehow, omitting this line causes errors)
    var nodes = ListBuffer.empty[ActorRef]
    for (keyPair <- keyPairs) {
      var node = blockchainSystem.actorOf(Node.props(keyPair), keyPair.getPublic.toString
        .replace("Sun RSA public key, 512 bits\n  modulus: ", "")
        .replace("public exponent: 65537", "").trim)
      if (counter == 0) node ! CreateGenesisBlock
      counter += 1
      nodes += node
    }

    lazy val nodesArray = nodes.toArray
    lazy val keyPairsArray = keyPairs.toArray
    lazy val rand = new Random(System.currentTimeMillis())
    for (i <- 1 to 10) {
      val node = nodesArray(rand.nextInt(nodesArray.size))
      node ! MakeTransaction(Map(keyPairs(rand.nextInt(keyPairs.size)).getPublic -> 10))
    }

    implicit def keyPairToTuple(p: KeyPair): Tuple2[PublicKey, PrivateKey] = (p.getPublic, p.getPrivate)

    try {
      StdIn.readLine()
    } finally {
      blockchainSystem.terminate()
    }
  }
}
