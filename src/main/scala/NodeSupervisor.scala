import java.security.{KeyPairGenerator, MessageDigest, PublicKey}

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

object NodeSupervisor {
  def props(): Props = Props(new NodeSupervisor)

  case object CreateNode
  final case class UpdateNodesList(nodes: Map[PublicKey, Int])
  final case class MakeUnconfirmedTransaction(nodeKey: PublicKey,
                                              destsAndAmounts: Map[PublicKey, Int])

  //TODO: messages that are sent to the node with the highest stake for validation

  //TODO: see how to distribute actors across different machines


}

//kept in case it's needed sometime in the future
class NodeSupervisor extends Actor with ActorLogging {
  override def receive: Receive = ???
  /*import NodeSupervisor._

  var nodeCounter: Int = 0
  var transCounter: Int = 0
  var blockCounter: Int = 0
  //maps public keys of the nodes to their respective balances
  //sent to all the nodes
  var nodes: Map[PublicKey, Int] = Map.empty[PublicKey, Int]
  //list of actor refs
  //kept inside supervisor
  //TODO: replace with message broadcasting to all the child actors
  var actorRefs: Seq[ActorRef] = Seq.empty[ActorRef]
  //unconfirmed transactions list
  var unconfirmedTransactions: Seq[Transaction] = Seq.empty[Transaction]
  //the blockchain itself
  var chain: Seq[Block] = Seq.empty[Block]
  //current inputs to for transaction formation
  var inputs: Seq[Int] = Seq.empty[Int]
  //TODO: place all the transactions here?
  var allTransactions: Seq[Transaction] = Seq.empty[Transaction]

  override def preStart() = {
    log.info("Supervisor node started")
    //add genesis block to the blockchain
    val timestamp = System.currentTimeMillis()
    chain :+ new Block(blockCounter,
      timestamp,
      0l,
      calculateHash(blockCounter, timestamp, "", "").toString,
      ""
    )
    blockCounter += 1
  }
  override def postStop(): Unit = log.info("Supervisor node stopped")

  //TODO: move most of logic to nodes
  override def receive = {
    case CreateNode =>
      val generator = KeyPairGenerator.getInstance("RSA")
      generator.initialize(256)
      val keys = generator.generateKeyPair()
      nodes + ((keys.getPublic, 0))
      val newNode = context.actorOf(Node.props(nodeCounter, 0, (keys.getPublic, keys.getPrivate)), "node" + nodeCounter)
      actorRefs :+ newNode
      actorRefs.iterator.foreach { nodeRef =>
        nodeRef ! UpdateNodesList(nodes)
      }
      log.info("Created new node: node" + nodeCounter)
      nodeCounter += 1
      //TODO: send publickey of new node to sender
    case MakeUnconfirmedTransaction(nodeKey, destsAndAmounts) =>
      val inputs = allTransactions.filter(t => t.output.keySet.contains(nodeKey)).map(t => t.id)
      val newTransaction = Transaction(transCounter, inputs, destsAndAmounts)
      transCounter += 1
      unconfirmedTransactions :+ newTransaction
      if (transCounter == 10)
        createBlock()
  }

  def calculateHash(index: Int, timestamp: Long, data: String, prevHash: String) = {
    MessageDigest.getInstance("SHA256").digest(
      (index.toString + timestamp.toString + data + prevHash).getBytes
    )
  }

  def createBlock() = {
    //TODO: validate transactions
    val timestamp = System.currentTimeMillis
    val prevBlockHash = chain.last.hash
    val newBlock = new Block(
      blockCounter,
      timestamp,
      0l, /*TODO: add hashed data?*/
      calculateHash(blockCounter, timestamp, "", prevBlockHash).toString,
      prevBlockHash
      )
    //TODO: valitade block (or not?)
    chain :+ newBlock
    blockCounter += 1
    unconfirmedTransactions = Seq.empty[Transaction]
    transCounter = 0
  }*/
}
