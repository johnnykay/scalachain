import java.security.{PrivateKey, PublicKey}
import javax.crypto.{Cipher}

object Encryptor {
  def encrypt(input: Array[Byte], key: PublicKey, xform: String): Array[Byte] = {
    val cipher: Cipher = Cipher.getInstance(xform)
    cipher.init(Cipher.ENCRYPT_MODE, key)
    cipher.doFinal(input)
  }

  def decrypt(input: Array[Byte], key: PrivateKey, xform: String): Array[Byte] = {
    val cipher: Cipher = Cipher.getInstance(xform)
    cipher.init(Cipher.DECRYPT_MODE, key)
    cipher.doFinal(input)
  }
}
