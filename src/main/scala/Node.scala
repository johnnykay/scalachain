import java.security.{MessageDigest, PrivateKey, PublicKey}

import akka.actor.{Actor, ActorLogging, Props}
import akka.util.Timeout
import akka.pattern.ask

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._

case class Block(number: Int,
                 timestamp: Long,
                 hash: String,
                 previousHash: String)

case class Transaction(id: Int,
                       src: PublicKey,
                       input: Seq[Int],
                       output: Map[PublicKey, Int])

object Node {
  def props(keyPair: (PublicKey, PrivateKey)): Props =
    Props(new Node(keyPair))

  final case class MakeTransaction(destsAndAmounts: Map[PublicKey, Int])

  case object QueryAllNodes

  final case class Handshake(publicKey: PublicKey, balance: Int)

  case object AskForCurrentState

  final case class Blockchain(chain: ListBuffer[Block], transactions: ListBuffer[Transaction])

  case object CreateGenesisBlock

  final case class AddTransaction(t: Transaction)

  case object MakeBlock

  final case class IsValid(message: Array[Byte])

  case object AllowTransactions
}

trait Counter {
  var count = 0

  def getAndIncrement: Int = {
    count += 1
    count
  }
}

object TransactionCounter extends Counter

object BlockCounter extends Counter

class Node(keyPair: (PublicKey, PrivateKey)) extends Actor  with ActorLogging {
  import Node._

  override def preStart(): Unit = {
    log.info("Created node: " + context.self.path)
    context.actorSelection("akka://blockchainSystem/user/router") ! QueryAllNodes
  }

  override def postStop(): Unit = log.info("Stopped node: " + context.self.path)

  //initial balance
  var balance: Int = 100
  //list of node private keys and balances
  var otherNodes: Map[PublicKey, Int] = Map.empty[PublicKey, Int]
  //list of transactions which have this node as their output
  var transactions: ListBuffer[Transaction] = ListBuffer.empty[Transaction]
  //the blockchain itself
  var chain: ListBuffer[Block] = ListBuffer.empty[Block]
  //are new transactions allowed?
  //stop until either new block is created or at least the necessity of it is checked
  var transactionsAllowed: Boolean = true

  def receive = {
    case QueryAllNodes =>
      log.info("Query received")
      sender() ! Handshake(keyPair._1, balance)
    case Handshake(publicKey, blnc) =>
      otherNodes += (publicKey -> blnc)
      if (otherNodes.size == 1)
        sender() ! AskForCurrentState
      log.info("Got handshake from " + publicKey.toString
        .replace("Sun RSA public key, 512 bits\n  modulus: ", "")
        .replace("public exponent: 65537", "").trim + "\nCurrent data: " + otherNodes)
    case Blockchain(chn, trnsctns) =>
      this.chain = chn
      this.transactions = trnsctns
      log.info("Node " + context.self.path + " received blockchain data from " + sender().path
        + "\nCurrent chain: " + this.chain + "\nCurrents transactions: " + this.transactions)
      transactionsAllowed = true
    case AskForCurrentState =>
      sender() ! Blockchain(chain, transactions)
    case CreateGenesisBlock =>
      val timestamp = System.currentTimeMillis()
      chain += Block(
        BlockCounter.getAndIncrement,
        timestamp,
        calculateHash(0, timestamp, ""),
        ""
      )
      log.info("Created genesis block:\nNumber:" + chain.head.number
        + "\nTimestamp:" + chain.head.timestamp
        + "\nHash:" + chain.head.hash
        + "\nPrevious hash:" + chain.head.previousHash)
      log.info("Chain state: " + chain)
    case MakeTransaction(destsAndAmounts) =>
      log.info("Transaction requested with output: " + destsAndAmounts)
      val newTransaction = Transaction(
        TransactionCounter.getAndIncrement,
        keyPair._1,
        transactions.filter(t => t.output.contains(keyPair._1)).map(t => t.id),
        destsAndAmounts
      )
      transactions += newTransaction
      otherNodes.foreach(n => context.actorSelection(n._1.toString.replace("Sun RSA public key, 512 bits\n  modulus: ", "")
        .replace("public exponent: 65537", "").trim) ! AddTransaction(newTransaction))
      log.info("Transaction " + newTransaction.id + " added to the unconfirmed transactions list")
      if (transactions.size % 10 == 0) {
        context.actorSelection(otherNodes.maxBy(_._2)._1.toString.replace("Sun RSA public key, 512 bits\n  modulus: ", "")
          .replace("public exponent: 65537", "").trim) ! MakeBlock
      } else {
        otherNodes.foreach(n => context.actorSelection(n._1.toString.replace("Sun RSA public key, 512 bits\n  modulus: ", "")
          .replace("public exponent: 65537", "").trim) ! AllowTransactions)
      }
    case MakeBlock =>
      log.info("Block creation started")
      var transactionsToConfirm = ListBuffer.empty[Transaction]
      transactions.foreach(t =>
        if(validateTransaction(t)) {
          transactionsToConfirm += t
        } else {
          log.info("Invalid transaction attempt: " + t.id)
          transactions.diff(Seq(t))
        }
      )
      val blockId = BlockCounter.getAndIncrement
      val timestamp = System.currentTimeMillis()
      chain += Block(
        blockId,
        timestamp,
        calculateHash(blockId, timestamp, chain.last.hash),
        chain.last.hash
      )
      log.info("Block " + blockId + " appended to the chain.\nCurrent blockchain: " + chain)
      recalculateBalances(transactionsToConfirm)
      otherNodes.keySet.foreach(key =>
        context.actorSelection(key.toString.replace("Sun RSA public key, 512 bits\n  modulus: ", "")
          .replace("public exponent: 65537", "").trim) ! Blockchain(chain, transactions)
      )
    case IsValid(message) =>
      sender() ! Encryptor.decrypt(message, keyPair._2, "RSA").toString.equals("message")
    case AddTransaction(transaction) =>
      transactions :+ transaction
      transactionsAllowed = false
    case AllowTransactions =>
      transactionsAllowed = true

  }

  def calculateHash(number: Int, timestamp: Long, previousHash: String): String = {
    MessageDigest.getInstance("SHA-256").digest(
      (number.toString + timestamp.toString + previousHash).getBytes("UTF-8")
    ).toString
  }

  def validateTransaction(transaction: Transaction): Boolean = {
    //check that each input transaction contains this transaction's src as an output
    transaction.input.foreach(index =>
      if(!transactions
        .filter(t => t.id == index)
        .head
        .output
        .keySet
        .contains(transaction.src))
        false
    )
    implicit val timeout: Timeout = Timeout(5 seconds)
    //send "secret message" to decode it with the private key
    transaction.output.keySet.foreach(key =>
      if (
        !(context.actorSelection(transaction.src.toString) ? IsValid(
          Encryptor.encrypt("message".getBytes("UTF-8"), key, "RSA"))).asInstanceOf[Boolean]
      )
        false
    )
    //check that the source node has enough funds for this transaction
    if(otherNodes.get(transaction.src).getOrElse(Int.MaxValue)
      < transaction.output.values.sum)
      return false //why do we need return?
    true
  }

  def recalculateBalances(transactions: Seq[Transaction]): Unit = {
    for ((node, balance) <- otherNodes) {
      transactions.foreach(t =>
        //TODO: check that the map pair actually gets replaced (according to docs, it should)
        otherNodes += (node -> (balance + t.output.getOrElse(node, 0)))
      )
    }
  }
}